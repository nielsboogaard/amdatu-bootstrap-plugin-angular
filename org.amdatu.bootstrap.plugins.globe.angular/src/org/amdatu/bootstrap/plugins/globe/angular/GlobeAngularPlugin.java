package org.amdatu.bootstrap.plugins.globe.angular;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.amdatu.web.api.WebResourcePlugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class GlobeAngularPlugin extends AbstractBasePlugin {
	
	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;

	@ServiceDependency
	private volatile WebResourcePlugin m_webResourcePlugin;

	private volatile BundleContext m_bundleContext;

	@Command
	public void globeApp() throws IOException, TemplateException {
		String appName = m_prompt.askString("What is the name of the app?");
		String viewPath = appName;//m_prompt.askString("What is the path of the default state?");
		
		String AppNameUppercase = Character.toUpperCase(appName.charAt(0)) + appName.substring(1);
		
		String view = m_prompt.askString("What is the file name of the default state partial?", viewPath + ".html");
		String controller = m_prompt.askString("What is the name of the controller for the default view?", AppNameUppercase + "Ctrl");
		String withService = m_prompt.askChoice("Do you want to create a service for this app?", 0, Arrays.asList("Yes", "No"));

		TemplateContext context = m_templateEngine.createContext();
		context.put("appName", appName);
		context.put("viewPath", viewPath);
		context.put("view", view);
		context.put("controller", controller);
		context.put("service", AppNameUppercase + "Service");

		String appPath = "web/" + appName;
		Files.createDirectories(m_navigator.getProjectDir().resolve(appPath));
		Files.createDirectories(m_navigator.getProjectDir().resolve(appPath + "/i18n"));
		Files.createDirectories(m_navigator.getProjectDir().resolve(appPath + "/images"));
		Files.createDirectories(m_navigator.getProjectDir().resolve(appPath + "/partials"));
		Files.createDirectories(m_navigator.getProjectDir().resolve(appPath + "/styles"));
		
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/" + appName + ".ts"),
				m_bundleContext.getBundle().getEntry("templates/app.vm"));
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/" + appName + ".ctrl.ts"), m_bundleContext.getBundle()
				.getEntry("/templates/controller.vm"));
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/partials/" + view),
				m_bundleContext.getBundle().getEntry("/templates/view.vm"));
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/styles/_" + appName + ".scss"),
				m_bundleContext.getBundle().getEntry("/templates/styles.vm"));
		
		URL translationTemplate = m_bundleContext.getBundle().getEntry("/templates/translations.vm");
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/i18n/en.json"),	translationTemplate);
		createFile(context, m_navigator.getProjectDir().resolve(appPath + "/i18n/nl.json"), translationTemplate);
		
		if ("Yes".equals(withService)) {
			createFile(context, m_navigator.getProjectDir().resolve(appPath + "/" + appName + ".service.ts"),
					m_bundleContext.getBundle().getEntry("/templates/service.vm"));
		}
		
	}

	private void createFile(TemplateContext context, Path outputFile, URL templateUri) throws TemplateException {
		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);

		String template = processor.generateString(context);
		try {
			Files.write(outputFile, template.getBytes(), StandardOpenOption.CREATE_NEW);
			System.out.println("Created file: " + outputFile);
		} catch (IOException ex) {
			System.out.println("Skipped file " + outputFile);
		}
	}
	
	@Override
	public String getName() {
		return "globe-app";
	}

	@Override
	public boolean isInstalled() {
		return false;
	}

}