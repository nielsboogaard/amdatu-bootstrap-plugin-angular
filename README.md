# Amdatu Bootstrap plugin - AngularJS app #

This plugin creates an AngularJS app within our web application in an interactive way.

### How do I get set up? ###

* Install [Amdatu Bootstrap](https://bitbucket.org/amdatu/amdatu-bootstrap)
* Run Amdatu Bootstrap:
```
#!bash
cd amdatu-bootstrap
java -jar bin/felix.jar
```
* Install this plugin using the gitinstaller:
```
#!bash
installFromUrl
https://bitbucket.org/nielsboogaard/amdatu-bootstrap-plugin-angular.git
```
* Check whether the installation succeeded by printing the installed plugins:
```
#!bash
listplugins
```